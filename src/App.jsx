//Create route for the navigation in the URL
import { BrowserRouter, Routes, Route } from "react-router-dom";
//
import Login from "./pages/Login";
import Profile from "./pages/Profile";
import Translation from "./pages/Translation";
import "./App.css";
import logo from './logo192.png';
import NavBar from "./components/Navbar/Navbar";


function App() {
  return (
    <BrowserRouter>
      <div className="App">
      <NavBar />
      <img src={logo} className="App-logo" alt="logo" />
       {/*Display the routes */}
        <Routes>
          <Route path="/" element={<Login/>} />
          <Route path="/translation" element={<Translation/>} />
          <Route path="/profile" element={<Profile/>} />
        </Routes>

      </div>
    </BrowserRouter>
  );
}

export default App;
