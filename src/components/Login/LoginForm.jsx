// Imports
import { useState, useEffect} from "react";
import { useForm } from "react-hook-form";
//Import loginUser function
import { loginUser } from "../../api/user";
import { storageSave } from "../../utils/storage";
import { useNavigate } from "react-router-dom";
import { useUser } from "../../context/UserContext";
import { STORAGE_KEY_USER } from "../../const/storageKeys";

// Required to have an username and an minimum of 2 characters
const usernameConfig = {
  required: true,
  minLength: 3,
};

// Create form for the login page
const LoginForm = () => {
  // Hooks
  const { register,handleSubmit, formState: { errors }, } = useForm();
  const {user, setUser} = useUser()
  const navigate = useNavigate()
  // Local state
  // Create loading message
  const [loading, setLoading] = useState(false);
  // API error message
  const [apiError, setApiError] = useState(null);

  // Side Effects

    useEffect(() =>{
      if(user !==null){

        navigate('Translation')
      }
    }, [user,navigate])

  // Event handlers


  // Render functions
  // Create event for new user
  const onSubmit = async ({ username }) => {
    setLoading(true);
    const [error, userResponse] = await loginUser(username);
    if (error !== null) {
      setApiError(error);
    }
    if (userResponse !== null) {
      storageSave(STORAGE_KEY_USER, userResponse);
      setUser(userResponse);
    }
    setLoading(false);
  };

  /*Handle errors and display them to the user */
  const errorMessage = (() => {
    if (!errors.username) {
      return null;
    }
    if (errors.username.type === "required") {
      return <span>Username is required</span>;
    }
    if (errors.username.type === "minLength") {
      return <span>Username is too short (min 3 characters)</span>;
    }
  })();

  return (
    <>
      <h2>What's your name?</h2>
      <form onSubmit={handleSubmit(onSubmit)}>
        <fieldset>
          <label htmlFor="username">Username:</label>
          <input
            type="text"
            placeholder="What's your name?"
            {...register("username", usernameConfig)}
          />
          {errorMessage}
        </fieldset>
        <button type="submit" disabled={loading}>
          Continue
        </button>
        {loading && <p>Login in...</p>}
        {apiError && <p>{apiError}</p>}
      </form>
    </>
  );
};

export default LoginForm;
