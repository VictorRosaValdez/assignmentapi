import { NavLink } from "react-router-dom"
import { useUser } from "../../context/UserContext"

const NavBar = () =>{

    const {user} = useUser()

    return(

        <nav>
        <ul>

            <li>Tanslations</li>
        </ul>
        { user !== null && 
            <ul>
            <li>
                <NavLink to="Translation">Translation</NavLink>
            </li>
            <li>
                <NavLink to="Profile">Profile</NavLink>
            </li>
        </ul>
        }
        </nav>

    )

}

export default NavBar