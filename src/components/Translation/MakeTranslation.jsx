


const MakeTranslation =()=>{

    let inputText = "Hello"
    Array.from(inputText);
    const signs = [
        {title: "a", src: "img/a.png"},
        {title: "b", src: "img/b.png"},
        {title: "c", src: "img/c.png"},
        {title: "d", src: "img/d.png"},
        {title: "e", src: "img/e.png"},
        {title: "f", src: "img/f.png"},
        {title: "g", src: "img/g.png"},
        {title: "h", src: "img/h.png"},
        {title: "i", src: "img/i.png"},
        {title: "j", src: "img/j.png"},
        {title: "k", src: "img/k.png"},
        {title: "l", src: "img/l.png"},
        {title: "m", src: "img/m.png"},
        {title: "n", src: "img/n.png"},
        {title: "o", src: "img/o.png"},
        {title: "p", src: "img/p.png"},
        {title: "q", src: "img/q.png"},
        {title: "r", src: "img/r.png"},
        {title: "s", src: "img/s.png"},
        {title: "t", src: "img/t.png"},
        {title: "u", src: "img/u.png"},
        {title: "v", src: "img/v.png"},
        {title: "w", src: "img/w.png"},
        {title: "x", src: "img/x.png"},
        {title: "y", src: "img/y.png"},
        {title: "z", src: "img/z.png"}
    

    ];
    

    
    return(

        <div>

            {signs.map((index) => <img src={index.src} title={index.title} key="signs-image" alt="images" height="30" width="30"/>)}
            
        </div>
            
        

    )
}

export default MakeTranslation