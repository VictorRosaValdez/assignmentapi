import TranslationHistoryItem from "./TranslationHistoryItem"

const TranslationHistory = ({translations}) =>{

    const translationsList = translations.map(
        (translation,index) =><TranslationHistoryItem key={ index + "-" +translation} translation={translation}/>
    )

    return(

        <>
        <h4>Your translations history</h4>
        <ul>
            {translationsList}
        </ul>
        </>
    )

}

export default TranslationHistory