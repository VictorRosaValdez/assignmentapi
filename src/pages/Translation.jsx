import MakeTranslation from "../components/Translation/MakeTranslation";
import TranslationActions from "../components/Translation/TranslationAction";
import TranslationHeader from "../components/Translation/TranslationHeader";
import TranslationHistory from "../components/Translation/TranslationHistory";
import { useUser } from "../context/UserContext";
import withAuth from "../hoc/withAuth";

function Translation (){


   
    const {user} = useUser()

    return (

        <>
        <h1>Translation</h1>
        <input type="text" placeholder="Enter words"></input>
        <TranslationHeader username={user.username}/>
        <TranslationActions />
        <TranslationHistory translations={user.translations}/>
        <div>
            <MakeTranslation/>
        </div>
        </>
    )


}

export default withAuth (Translation)